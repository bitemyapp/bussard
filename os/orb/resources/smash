-- -*- lua -*-

local f, orig_env, args = ...
local env = orb.utils.shallow_copy(orig_env)

if(args[1] == "--help") then
   print("An interactive command shell.\n")
   print("Usage:")
   print("  smash")
else
   local smashrc = f[env.HOME .. "/.smashrc"]
   if(smashrc) then
      assert(loadstring(smashrc))()
   else
      print("\n" .. f.etc.motd .. "\nType \"logout\" to exit and \"ls /bin\"" ..
               " to see commands available.")
   end

   while true do
      set_prompt(orb.utils.interp(env.PROMPT, env))
      local input = orb.expand_globs(f, orb.utils.interp(io.read(), env), env)
      if not input or input == "exit" or input == "logout" then return end

      local var, value = input:match("export (.+)=(.*)")
      local change_dir = input:match("cd +(.+)")
      change_dir = change_dir and orb.normalize(change_dir, env.CWD)

      -- inlining primitives this way is kinda tacky
      if(input == "cd") then
         env.CWD = env.HOME
      elseif(change_dir and not f[change_dir]) then
         print(change_dir .. " not found.")
      elseif(change_dir and type(f[change_dir]) ~= "table") then
         print(change_dir .. " is not a directory")
      elseif(change_dir) then
         env.CWD = change_dir
      elseif(var) then
         env[var] = value
      elseif(not input:match("^ *$")) then
         local success, msg = orb.pexec(f, env, input, orb.extra_sandbox)
         if(not success) then
            print(msg)
            env.LAST_ERROR = msg
         else
            env.LAST_ERROR = nil
         end
      end
   end
end
