-- This -*- lua -*- code runs inside your ship's own computer.

-- controls are for keys that need to keep activating when held down; bind
-- is for commands that only call their functions once even when held
ship.controls = {
   up = ship.actions.forward,
   left = ship.actions.left,
   right = ship.actions.right,
   ["="] = function(d) if d then ship.scale = ship.scale - (ship.dt/2) end end,
   ["-"] = function(d) if d then ship.scale = ship.scale + (ship.dt/2) end end,
}

-- Flight mode
define_mode("flight")

bind("flight", "escape", ship.ui.pause)
bind("flight", "ctrl-return", lume.fn(ship.editor.change_buffer, "*console*"))

-- ctrl-tab selects closest target; repeated ctrl-tab selects next-closest
bind("flight", "ctrl-tab", ship.actions.closest_target)

-- regular tab selects next target in "order"
bind("flight", "tab", ship.actions.next_target)

dofile("src.hud") -- heads-up display during flight mode

-- other modes
dofile("src.edit") -- editor
dofile("src.mail") -- mail client
dofile("src.console") -- console for interacting with ship's computer
dofile("src.ssh") -- ssh mode for interacting with station/planet computers

-- multi-mode bindings

bind({"flight", "edit"}, "ctrl-q", ship.ui.quit)

-- open mail client
bind({"flight", "edit"}, "ctrl-m", function()
        ship.editor.change_buffer("*console*")
        mail()
end)

-- to open a file
bind({"flight", "edit"}, "ctrl-o", function()
        ship:read_line("Open: ", lume.fn(ship.editor.open, ship))
end)

-- reload config
bind({"flight", "edit"}, "ctrl-r",
   -- maybe bind this to something that isn't already used by edit mode
   function()
      local success, err = ship.dofile("src.config")
      if(success) then
         ship.editor.initialize()
         print("Successfully reloaded config.")
      else
         print("Error loading config.")
      end
end)

-- connect to target over ssh
bind("flight", "ctrl-s", function()
        ship.editor.change_buffer("*console*")
        ssh()
end)

bind("flight", "ctrl-p", portal)

-- if you keep your personal customizations in here, they will be preserved
-- in the event of a config reset.
-- dofile("src.custom")
