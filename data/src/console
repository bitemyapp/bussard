-- -*- lua -*-

define_mode("console")
ship.modes.console.parent = ship.modes.edit -- most keys work like in edit mode
ship.modes.console.textinput = ship.modes.edit.textinput

bind("console", "ctrl-a", ship.editor.beginning_of_input)
bind("console", "home", ship.editor.beginning_of_input)

bind("console", "alt-p", ship.editor.history_prev)
bind("console", "alt-n", ship.editor.history_next)
bind("console", "ctrl-up", ship.editor.history_prev)
bind("console", "ctrl-down", ship.editor.history_next)

bind("console", "return", function()
        if(ship.editor.get_line_number() ~= ship.editor.get_max_lines()) then
           ship.editor.end_of_buffer()
           return
        end

        local input = utf8.sub(ship.editor.get_line(0),
                               utf8.len(ship.editor.prompt())+1)
        ship.editor.history_push(input)
        ship.editor.end_of_line()
        ship.editor.newline()
        local chunk, err = loadstring("return " .. input)
        if(err and not chunk) then -- statement, not expression
           chunk, err = loadstring(input)
           if(not chunk) then
              print("! Compilation error: " .. err or "Unknown error")
              ship.editor.print_prompt()
              ship.editor.end_of_buffer()
              return false
           end
        end
        local trace
        local result = pack(xpcall(chunk, function(e)
                                      trace = debug.traceback()
                                      err = e end))
        if(result[1]) then
           local output, i = pps(result[2]), 3
           if result[2] == ship.editor.invisible then
              ship.editor.print_prompt()
              return true
           end
           while i <= #result do
              output = output .. ', ' .. pps(result[i])
              i = i + 1
           end
           print(output)
        else
           realprint('! Evaluation error: ' .. err or "Unknown")
           local lines = lume.split(trace, "\n")
           for i,l in pairs(lines) do
              -- editor infrastructure wraps 8 levels of irrelevant gunk
              if(i < #lines - 8) then realprint(l) end
           end
        end
        ship.editor.print_prompt()
end)

local complete = function()
   local point, point_line = ship.editor.point()
   if(point_line ~= ship.editor.get_max_lines()) then return end
   local line = utf8.sub(ship.editor.get_line(0),
                         utf8.len(ship.editor.prompt())+1, point)
   local entered = lume.last(lume.array((line):gmatch("[._%a0-9]+"))) or ""
   local completions = utils.completions_for(entered, _G, ".")
   if(#completions == 1) then
      ship.editor.textinput(utf8.sub(completions[1], #entered + 1))
   elseif(#completions > 0) then
      local common = utils.longest_common_prefix(completions)
      if(common == entered) then
         print(table.concat(completions, " "))
      else
         ship.editor.textinput(utf8.sub(common, #entered + 1))
      end
   end
end

bind("console", "tab", complete)
bind("console", "ctrl-i", complete)
