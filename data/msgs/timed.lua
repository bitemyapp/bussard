return {
                    [-1] = "manual.msg",
                     [0] = "readme.msg",
                  [5000] = "strauss-1.msg",
                [100000] = "passenger1.msg",
                [200000] = "passenger2.msg",
               [1200000] = "passenger3.msg",
   [{"luabook",  20000}] = "nari-decrypt-01.msg",
   -- TODO: might be better to deliver this without requiring a portal
   [{"luabook", 120000}] = "nari-decrypt-02.msg",
}
