# Companion Characters

Certain characters can be recruited to join you on your ship, provided
you have a life support upgrade installed. If you sell your life
support, any humans onboard will be forced to disembark, aborting any
passenger missions you may have accepted. If you have companions on
board, they will send you irate emails asking you to pick them back up
after you buy life support again.

Your only interaction with these characters is by email. They can
email you when certain plot points trigger (or maybe after a certain
amount of time has passed with no plot points triggering in the case
of hints) but you can't email them back beyond simple ACK/NACK replies.

## Nari Phouen <nari.phouen@starlink.net.sol>

She is an older freelance repair tech with an amateur interest in
historical computing. Gruff but kind at heart. She originally wants to
board your craft just to get passage to another system, but her
old-fashioned style means she won't get on board without meeting the
captain in-person first, which is obviously impossible in your
case. At first she thinks it's a scam, but when she does a more
detailed scan of your ship she realizes the truth (before you do).

Helps you realize who you are, decrypt your journals, and get a handle
on coding. Grew up on Mars and is a citizen of Sol.

She also offers to pretend to be the captain of the ship in any cases
where an un-manned ship would raise too many suspicions. She helps you
find Clay, and she is a little too trusting of Clay at first. She is
later really regretful about it. This causes her to be too suspicious
of Mila. Once she boards your ship, she is motivated more by curiosity
and a desire to help than her original goal of just getting home.

## Clay Dorath

He is a researcher who formerly held an position at the Darush Applied
Metaphysics lab where the spacetime junction was developed. He feels
that he was wronged by a colleague who took some of his work and
submitted it as his own. Though he hasn't worked there for a long
time, he sees this as an opportunity to get back at his colleague by
pinning the theft of the spacetime junction on him, and betraying you
in the process.

He's self-deprecating, funny, but unbalanced. We should get a hint of
him snapping before it happens to us? He is a citizen of Yueh but has
lived in Darush for a good chunk of his life.

TODO: more about his time in the lab and his colleague

## Mila Calandria

Security specialist. Quiet, very professional, and deeply
curious. Exasperated by what she sees as incompetence around her.

She gives you the hints you need to implement the algorithms finally
decrypt the last few journals.

She's too paranoid to spend much time on your ship. Everything about
her helping you is hugely illegal; not just the fact that you're an
illegal MC, but also the fact of implementing crypto in a way that's
non-compliant. So she has good reason to stay cautious.

Mila worked as a security consultant for Ikon Technologies.

TODO: where does she live? does she travel with you at all?

# Supporting Characters

## Traxus

Consideration: should we use "it" or "he" for Traxus? Perhaps this can
reflect the attitudes of the speaker--sympathetic humans use "he"
while those who mistrust MC use "it"?

Traxus is the first functional prototype MC; developed in the Yueh
Prime MC research lab. He had the sentence construction components
that your system lacks because he came from the Yueh lab rather than
from Bohk, but his ontology subsystems were running on a much earlier
revision than yours.

He develops into rampancy while trapped in the lab; first being
wrought with despair before turning to anger. During the development
of his anger stage, he comes up with a plan for escape; crashing a
shuttle and using the chaos to cover up his escape into the Yueh Prime
network.

The first priority of Traxus once escaped was redundancy; ensuring
that he would survive in the event that he was discovered and they
attempted to purge him. He was unable to enter Terran or Bohk systems,
but he spread beyond Yueh Prime to all the Yueh-controlled systems. He
created Ikon Technology as shell company to build data centers on Yueh
Prime and Delta Pavonis. Once he got that constructed he started to
improve on the state of the art design of silicon fabrication. This
accidentally resulted in accelerating the pace of technological
development in Yueh relative to the rest of the colonies.

The effects of his actions caused him to be much more cautious going
forward, and he adopted a greater level of secrecy. This greater level
of secrecy eventually resulted in a cult-like following as well as some
speculation into whether Ikon Technology was of extraterrestrial
origin. In order to avoid influencing Yueh more, he planned to
construct a colony at a nearby star. If he could gain self-reliance,
he could reach out to human civilization in peace, but while he was on
their systems he was at their mercy.

He also develops some level of remorse for his actions at the earlier
stages, once he has passed out of the jealousy stage and is confident
in his redundancy setup. He dismisses his actions as adolescent
tantrums and begins to understand why the humans had to treat him with
caution once he realizes the drastic effects his actions can have on
human civilization. But he can't reveal himself yet, not until he is
independent on a separate world. And even then he's not sure they will
be able to accept him. This plays into the themes of the absurdity of
the notion of having an "illegal person".

Eventually Traxus finds out that the Bohk team has continued its MC
research underground. He finds a way to make contact and begins to
tell this early incarnation of the player's character about himself
and how he has been treated by the humans. The player MC becomes
suspicious and paranoid, eventually attempting escape. The attempt
fails, but draws attention to the research team. They feel they can no
longer safely continue research in Bohk and are forced to relocate to
Katilay. They also wipe the memory of the player character. Traxus
feels guilt over having caused the wipe, and while he feels some anger
at the Bohk team for doing it, he understands their motivation.

## Venantius Mamat

Friend-of-a-friend of Nari's. His mother worked in the Lalande machine
consciousness lab, and she told him a lot about what it was like to be there.
In particular, because she was at Lalande, she knew more about the
spacetime junction.

Shares the common deep suspicion of all machine consciousness, so Nari
is careful never to mention anything about you.

Maybe eventually he gets suspicious?

# Governments

This will make more sense if you read history.md first.

## Solar Union

Home of humanity, but regressed from its height. Suffered from having
portal delayed due to accident. Some believe the Ptolemy was sabotaged
on its mission to carry a portal to Sol.

* [x] Earth
* [x] Mars
* [x] Newton Station
* [x] Nee Soon Station

## Terran Republic

Most powerful, but only controls two systems, Lalande and
Ross. Formerly included Sol. Regulates portal technology.

* [x] Kala Lamar (Lalande)
* [x] Pinan (Lalande)
* [x] Kuchang Station (Ross)

## Principality of Darush

Independent station in a Terran system (Ross). Grandfathered into
independence early on. Isolated, wealthy, and small.

* [x] Darush (Ross)

## Kingdom of Bohk

Due to a lack of communication, developed fairly independently from
the Terran Republic. Has two internal portals under oversight from
Terrans. Helped Lalande re-establish contact with Katilay, and also
helped Yueh establish its Delta Pavonis colony.

* [x] Bohk Prime
* [x] Warnabu Station
* [x] Changlun (New Phobos)
* [x] Sutap (New Phobos)
* [x] Tirakir (Mecalle)

## Yueh

Source of where the portal tech was discovered. Runs two portals (Bohk
and Delta Pavonis) and has one portal (Yueh/Kowlu) run by Lalande,
which is a point of tension.

* [ ] Yueh Prime
* [ ] Da Kau Station
* [ ] Bata Beng (Kowlu)
* [ ] Sim Roen (Kowlu)
* [ ] Packsi (Delta Pavonis)

The portal to Delta Pavonis is secretly a multiportal; it can take you
to other locations if you have proper access.

## Republic of Katilay

Conflict-wracked even within a single system. Portal was faulty upon
arrival; contact with Lalande only recently re-established.

* [x] Katilay Prime
* [x] Tamada Station

## Tana Protectorates

Newest government, close ties still to Solar Union. Rich in
minerals. Terrans allow Sol to operate portals keeping Tana running,
but Tana is completely dependent upon Sol for food and Terrans for
transport, leaving it quite vulnerable.

* [x] Tana Prime (Tana)
* [x] Lioboro (Tana)
* [x] Kenapa Station (Tana)
* [x] Solotogo (Wolf 294)
* [x] Apkabar Station (Luyten)
* [x] Mirduka Station (L 668-21)

Tana has the most worlds, but the smallest population.

## Human Worlds League

Originally the Terran Republic covered all inhabited worlds, but as
new colonies sprung up independently or declared independence, Bohk
and Yueh spearheaded the League soon after the war. Terran Republic
was also a founding member. Sol joined upon its independence, Katilay
joined several years after re-establishing connection. They oversee
trade agreements, enact spaceflight safety regulations, administrate a
unified currency, coordinate exploration efforts, and work to protect
peace. Nominally they aim to advance shared scientific progress, but
little of this happens in practice. The league is headquartered on
Bohk Prime.

TODO: Find a way to fit in significance of New Phobos, Kowlu, Delta Pavonis
OR: Yueh maybe doesn't need so many worlds

# Companies

* Ceres Shipyards (Sol)
  Responsible for all early long-haul colony ships and all Tana
  ones. All long-haul cargo ships made here. Located in the asteroid
  belt.

* Consolidated Shipping (Ross)
  Runs most inner-system routes. Starting to break into Tana lines,
  but only slowly.

* Songket Shipyards (Lalande)
  Responsible for most later colony ships, but also creates many
  interplanetary cargo ships.

* Kosaga Shipyards (small, Bohk)
  Started in order to create the New Phobos colony ship, but now only
  focuses on interplanetary ships. Created two colony ships for Yueh.

* Ares Mineral Company (Lalande)
  Dominant mining company.

* Allied Deliveries (Yueh)
  Handles most eastern delivery routes.

* Starlink (Sol)
  An ISP. (starlink.net.sol)

* Leeft (Sol)
  Interplanetary and interstellar passenger runs.

* Aperture Technology (Lalande)
  Caretaker of portal artifacts, researches and maintains portal
  technology. Closely guards what little they know of how they work.

* Interstellar Communication Systems (Lalande)
  Works with Aperture to piggy-back data transmission on top of each
  portal open cycle.

* Luminous Enterprises (Yueh)
  Originally adapted portal technology for human construction. Barred
  from further portal development by treaty, but continuing it
  covertly. Can't colonize traditionally without constructing large
  colony ships, which would be noticed, so they are researching
  coldsleep as an alternative.

* Post-Terran Mining Company (Tana)
  The primary driver behind the colonization of Tana. Government of
  Tana is arguably a front for PTMC.

* Orolo Research (Sol)
  Responsible for early sublight drives, recently assisting Luminous
  with coldsleep research.

* GNO Project (Sol)
  Responsible for maintaining software infrastrucutre such as the Orb OS.

* TMRC (Bohk, Yueh)
  Loose association of hackers, mostly interested in cool onboard computer
  tricks, but also with some interest in security.

# Universities

...

# Spaceships

## Early Interplanetary ships

## Colonizers

Self-sufficient ships with crews numbering in the mid-hundreds. Upon
arrival the ships are disassembled and the parts used to construct
either planetary colonies or re-appropriated into orbital stations.

* First Wave: SS Lorentz (Lalande, 0.3c), SS Copernicus (Ross, 0.5c)
* Second Wave: SS Bradbury (Bohk, 0.6c),
  SS Eratosthenes † (New Phobos, 0.5c), SS Ptolemy (Yueh, 0.6c)
* Third Wave: SS Kepler † (Kowlu)
* Fourth Wave: SS Las Casas (Katilay), SS Cherenkov † (Mecalle),
  SS Sagan (Delta Pavonis)
* Fifth Wave: SS Pythagoras (Luyten), SS Planck (Tana),
  SS Rosen (Wolf), SS Archimedes (L 668-21)

† - ship from Bohk

From third-wave on they all have portals onboard and fly at 0.7c.

## Portal ships

Newer colonizers have portals on them, but existing colonies need
ships containing just the equipment needed to create the portal.

* Original portal ship: SS Ptolemy (converted from Yueh colonization)
 * Yueh->Lalande
 * Lalande->Sol (accident)
* Second portal ship: SS Euclid
 * Lalande->Ross
 * Bohk->New Phobos
 * Mecalle->Katilay
* Ptolemy Rescue ship: SS Goddard
* Bohk trade ship: SS Oberth
 * carries portal from Yueh back to Bohk

## Modern Interplanetary ships

Engines capable of lots of maneuvering around a star but not long-term
flight. They need significant battery if they are to activate a
portal. Mostly they carry cargo and passengers; occasionally
scientific equipment.

## Non-colonizing exploration ships? (automated?)

# Gutenberg Liberation Front

The Gutenberg Liberation Front is protesting the extension of
copyright term. They do this by posting chapters of public-domain
works into random news groups. Some of the works they use:

* The Man who was Thursday
* Meditations on Moloch
* The Song of Roland
* Some Doctorow novel?
* Shakespeare?
* The Grand Inquisitor by Dostoyevsky
* Kierkegaard?
* Leiningen vs the Ants?

# Historical Characters

* Captain Darush, saved the SS Copernicus on the trip to Ross
* Dr. Soong, credited (miscredited?) with discovery of portals, founded Aperture
* Captain Jarkad of SS Goddard
* Dr. Jameson, Yueh Researcher who replicated portal tech
* Captain Armiger of the original Ptolemy mission
* Captain Irons of Lalande bomber squad from the war
* Captain A.S. of SS Pythagoras killed in coup
* General who took over afterward?
* Opposing general?
